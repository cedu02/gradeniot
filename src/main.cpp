#include <HardwareSerial.h>
#include <Temperature/TemperatureSensor.h>
#include <Wire.h>
#include "../lib/SerialCommunication/SerialCommunication.h"
#include "../lib/Devices/Fan.h"
#include "../lib/Devices/Pump.h"
#include "../lib/Devices/Lamp.h"
#include <RTClib.h>

void setup() {
    Serial.begin(9600);
    Fan::registerFans();
    Pump::registerPump();
    Lamp::registerLamp();
    Serial.println("Started");
}

void loop() {
    TaskEnum task = SerialCommunication::getTask();
    switch (task){
        case TaskEnum::StartFan:
            Fan::startFans();
            SerialCommunication::sendResult("true");
            break;
        case TaskEnum::StartPump:
            Pump::startPump();
            SerialCommunication::sendResult("true");
            break;
        case TaskEnum::StopPump:
            Pump::stopPump();
            SerialCommunication::sendResult("true");
            break;
        case TaskEnum::StopFan:
            Fan::stopFans();
            SerialCommunication::sendResult("true");
            break;
        case TaskEnum::StartLight:
            Lamp::startLamp();
            SerialCommunication::sendResult("true");
            break;
        case TaskEnum::StopLight:
            Lamp::stopLamp();
            SerialCommunication::sendResult("true");
            break;
        case TaskEnum::ReadWaterLevelReservoir:
            break;
        case TaskEnum::ReadWaterLevelTray:
            break;
        case TaskEnum::ReadHumidityPlants:
            break;
        case TaskEnum::NoOpenTask:
            break;
        case TaskEnum::ReadTemperatureHumidity:
            TemperatureSensor sensor(2);
            SerialCommunication::sendResult(sensor.readDataForService());
            break;
    }
    //Serial.flush();
}
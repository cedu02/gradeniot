#include <Arduino.h>
#include <unity.h>
#include <WaterLevel/WaterLevelSensor.h>


void test_led_state_high(void) {
    analogWrite(A0, 300);
    digitalWrite(LED_BUILTIN, HIGH);
    TEST_ASSERT_EQUAL(WaterLevel::Normal,  analogRead(A0));
}

void setup(){
    delay(2000);
    Serial.begin(9600);     // Communication started with 9600 baud
}
void loop(){
    /*int sensor = analogRead(A0); // Incoming analog signal read and appointed sensor
    switch(WaterLevelSensor::getWaterLevel(sensor)){
        case WaterLevel::Normal:
            Serial.println("Normal");
            break;
        case WaterLevel::Low:
            Serial.println("Low");
            break;
        case WaterLevel::Empty:
            Serial.println("Empty");
            break;
    }*/
    RUN_TEST(test_led_state_high);
    delay(500);
    UNITY_END(); // stop unit testing
}
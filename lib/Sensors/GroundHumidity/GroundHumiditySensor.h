//
// Created by cedu on 3/16/20.
//

#ifndef STRAWBERRZ_GROUNDHUMIDITYSENSOR_H
#define STRAWBERRZ_GROUNDHUMIDITYSENSOR_H


#include <HID.h>

class GroundHumiditySensor {
public: static int getGroundHumidity(int value);
};

#endif //STRAWBERRZ_GROUNDHUMIDITYSENSOR_H

//getWaterLevel
// Created by cedu on 3/16/20.
//

#include "GroundHumiditySensor.h"

int GroundHumiditySensor::getGroundHumidity(int value) {
    int result = constrain(value,400,1023);	//Keep the ranges!
    result = map(result,400,1023,100,0);
    return result;
}

//
// Created by cedu on 3/16/20.
//
#include <stdint-gcc.h>
#include <HID.h>
#include "WaterLevelSensor.h"

WaterLevel WaterLevelSensor::getWaterLevel(int value) {
    if(WaterLevel::Normal < value){
        return WaterLevel::Normal;
    }else if(value <= WaterLevel::Empty){
        return WaterLevel::Empty;
    }else {
        return WaterLevel::Low;
    }
}


//
// Created by cedu on 3/16/20.
//

#ifndef STRAWBERRZ_WATERLEVELSENSOR_H
#define STRAWBERRZ_WATERLEVELSENSOR_H

enum WaterLevel: int{
    Normal = 250,
    Low = 200,
    Empty = 100
};

class WaterLevelSensor {
public: static WaterLevel getWaterLevel(int value);
};

#endif //STRAWBERRZ_WATERLEVELSENSOR_H

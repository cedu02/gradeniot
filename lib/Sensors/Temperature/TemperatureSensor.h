//
// Created by cedu on 3/22/20.
//

#ifndef STRAWBERRZ_TEMPERATURESENSOR_H
#define STRAWBERRZ_TEMPERATURESENSOR_H

#include <WString.h>
#include <DHT.h>

class TemperatureSensor {
public:
    TemperatureSensor(int pin);
    String readDataForService();

private:
    float readTemperature();
    float readHumidity();
    DHT sensor;
};
#endif //STRAWBERRZ_TEMPERATURESENSOR_H

//
// Created by cedu on 3/22/20.
//

#include "TemperatureSensor.h"
float TemperatureSensor::readHumidity() {
    return this->sensor.readHumidity();
}
float TemperatureSensor::readTemperature() {
    return this->sensor.readTemperature(false);
}

String TemperatureSensor::readDataForService() {
    delay(500);
    return (String) String(this->readTemperature(), 2)+ ";" + String(this->readHumidity(), 2);
}

TemperatureSensor::TemperatureSensor(int pin): sensor(pin, DHT11) {
    this->sensor.begin();
}

//
// Created by cedu on 3/22/20.
//

#ifndef STRAWBERRZ_TASKENUM_H
#define STRAWBERRZ_TASKENUM_H

enum class TaskEnum{
    StartPump = 1,
    StopPump = 2,
    StartFan = 3,
    StopFan = 4,
    StartLight = 5,
    StopLight = 6,
    ReadTemperatureHumidity = 7,
    ReadWaterLevelReservoir = 8,
    ReadWaterLevelTray = 9,
    ReadHumidityPlants = 10,
    NoOpenTask = 999
};
#endif //STRAWBERRZ_TASKENUM_H

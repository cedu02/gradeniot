//
// Created by cedu on 3/18/20.
//

#ifndef STRAWBERRZ_SERIALCOMMUNICATION_H
#define STRAWBERRZ_SERIALCOMMUNICATION_H
#include <HID.h>
#include "TaskEnum.h"

class SerialCommunication {
public:
 static TaskEnum getTask();
 static void sendResult(String result);

private:
    static String readSerialPort();
    static char startMarker;
    static char endMarker;
};
#endif //STRAWBERRZ_SERIALCOMMUNICATION_H

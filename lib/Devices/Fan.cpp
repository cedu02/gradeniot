//
// Created by cedu on 4/5/20.
//

#include <HID.h>
#include "Fan.h"

#define in1 6
#define in2 7
#define in3 8
#define in4 9


void Fan::startFans() {
    digitalWrite(in1, HIGH);
    digitalWrite(in2, LOW);

    digitalWrite(in3, HIGH);
    digitalWrite(in4, LOW);
}

void Fan::stopFans() {
    digitalWrite(in1, LOW);
    digitalWrite(in2, LOW);

    digitalWrite(in3, LOW);
    digitalWrite(in4, LOW);
}

void Fan::registerFans() {
    pinMode(in1, OUTPUT);
    pinMode(in2, OUTPUT);
    pinMode(in3, OUTPUT);
    pinMode(in4, OUTPUT);
    stopFans();
}
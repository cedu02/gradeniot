//
// Created by cedu on 4/6/20.
//

#ifndef STRAWBERRYSENSORS_PUMP_H
#define STRAWBERRYSENSORS_PUMP_H


class Pump {
public:
    static void startPump();
    static void stopPump();
    static void registerPump();
};


#endif //STRAWBERRYSENSORS_PUMP_H

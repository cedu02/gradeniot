//
// Created by cedu on 4/6/20.
//

#ifndef STRAWBERRYSENSORS_LAMP_H
#define STRAWBERRYSENSORS_LAMP_H


class Lamp {
public:
    static void startLamp();
    static void stopLamp();
    static void registerLamp();
};


#endif //STRAWBERRYSENSORS_LAMP_H

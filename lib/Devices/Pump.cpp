//
// Created by cedu on 4/6/20.
//

#include <HID.h>
#include "Pump.h"
#define in1 5

void Pump::startPump() {
    digitalWrite(in1, HIGH);
}

void Pump::stopPump() {
    digitalWrite(in1, LOW);
}

void Pump::registerPump() {
    pinMode(in1, OUTPUT);
}

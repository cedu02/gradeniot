//
// Created by cedu on 4/6/20.
//

#include <HID.h>
#include "Lamp.h"
#define in1 4

void Lamp::startLamp() {
    digitalWrite(in1, HIGH);
}

void Lamp::stopLamp() {
    digitalWrite(in1, LOW);
}

void Lamp::registerLamp() {
    pinMode(in1, OUTPUT);
}